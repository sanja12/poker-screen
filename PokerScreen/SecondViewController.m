//
//  SecondViewController.m
//  PokerScreen
//
//  Created by Sanja Pantić on 24/08/19.
//  Copyright © 2019 Sanja Pantić. All rights reserved.
//

#import "SecondViewController.h"
#import "FirstViewController.h"
#import "Card.h"

@implementation SecondViewController
@synthesize bet, allMoney;

NSMutableArray *deck2;
Card *card;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self printInfo];
    
    _credit.enabled = NO;
    
}

- (IBAction)low:(id)sender {
    
    card = [self chooseRndCard];
    
    NSString *img = [card.image accessibilityIdentifier];
    
    _cardImage.image = [UIImage imageNamed:img];
        
    if (card.num <= 7 && card.num > 1) {
        bet *= 2;
        
        [self printInfo];
        
    } else {
        bet = 0;
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
}

- (IBAction)hi:(id)sender {
    
    card = [self chooseRndCard];
    
    NSString *img = [card.image accessibilityIdentifier];
    
    _cardImage.image = [UIImage imageNamed:img];
    
    if (card.num > 7 || card.num == 0) {
        bet *= 2;
        
        [self printInfo];
        
    } else {
        bet = 0;
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
}

- (IBAction)finish:(id)sender {
    
    allMoney += bet;
    
}

- (Card *) chooseRndCard {
    
    FirstViewController *fvc = [[FirstViewController alloc]init];
    
    deck2 = [fvc makeDeck];
    
    int rd = arc4random_uniform(52);
        
    return deck2[rd];

}

- (void) printInfo {
    
    _money.text = [NSString stringWithFormat:@"BET $: %d", bet];
    
    NSString *title = [NSString stringWithFormat:@"CREDIT$: %d", allMoney];
    
    [_credit setTitle:title forState:UIControlStateNormal];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier]isEqualToString:@"i2"]) {
        FirstViewController *fvc = [segue destinationViewController];
                
        fvc.allMoney = allMoney;
        
    }
}


@end


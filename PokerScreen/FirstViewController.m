//
//  FirstViewController.m
//  PokerScreen
//
//  Created by Sanja Pantić on 24/08/19.
//  Copyright © 2019 Sanja Pantić. All rights reserved.
//

#import "FirstViewController.h"
#import "Card.h"
#import "SecondViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController
@synthesize bet, allMoney;

int stake = 100;
bool isFirstDeal = YES;
int score = 0;

NSMutableArray *deck;
NSMutableArray *cardsForReplace;
NSMutableArray *ourFiveCards;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self makeDeck];
    
    cardsForReplace = [[NSMutableArray alloc] initWithCapacity:5];
    ourFiveCards = [[NSMutableArray alloc] initWithCapacity:5];
    
    _gambleGame.enabled = NO;
    _finishBtn.enabled = NO;
    
    bet = 0;
    
    // if we start game money is 1000; and if we return from second screen money is money that we save
    // on second screen
    
    if(!allMoney) {
        allMoney = 1000;
    }
    
    [self printMoneyInfo3];
    
    [self showHideHeldLbls:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)replace0:(UIButton *)sender {
    
    [self replaceOneCard:sender andLbl:_held0];
    
}

- (IBAction)replace1:(UIButton *)sender {
    
    [self replaceOneCard:sender andLbl:_held1];

}

- (IBAction)replace2:(UIButton *)sender {
    
    [self replaceOneCard:sender andLbl:_held2];

}

- (IBAction)replace3:(UIButton *)sender {
    
    [self replaceOneCard:sender andLbl:_held3];

}

- (IBAction)replace4:(UIButton *)sender {
    
    [self replaceOneCard:sender andLbl:_held4];
    
}

// Bet all money that we have

- (IBAction)betAll:(id)sender {
    
    if([self isMoneyZero] || !isFirstDeal) {
        return;
    }
    
    stake = allMoney;
    
    [self printMoneyInfo3];
}

// Sum bet money with our credit

- (IBAction)finishGame:(id)sender {
    
    [self disableEnableMainBtns:YES];
    
    allMoney += bet;
    
    bet = 0;
    
    stake = 100;
    
    [self printMoneyInfo3];
    
}

- (IBAction)dealCards:(id)sender {
    
    _gambleGame.enabled = NO;
        
    if(allMoney == 0 && isFirstDeal) {
        [self isMoneyZero];
        return;
    }
    
    if(isFirstDeal) {
        
        allMoney -= stake;
        
        [self showCards];
        
        isFirstDeal = NO;
        [self disableEnableBtns:YES];
        [self showHideHeldLbls:NO];
        
    } else {
        
        if(cardsForReplace.count > 0) {
            
            [self replaceCards];
            cardsForReplace = [[NSMutableArray alloc] initWithCapacity:5];
        }
        
        isFirstDeal = YES;
        [self disableEnableBtns:NO];
        [self showHideHeldLbls:YES];
    }
    
    score = [self calculateWin];
        
    bet = stake / 100 * score;
     
    if (isFirstDeal) {
        
        stake = 100;
        
        if(score > 0) {
            
            [self disableEnableMainBtns:NO];
            
        }
        
    }
        
    [self printMoneyInfo2];
    
    if(allMoney == 0 && bet == 0 && isFirstDeal) {
        [self isMoneyZero];
    }
}

- (IBAction)goToGambleGame:(id)sender {
    
    [self disableEnableMainBtns:YES];
    
    [self printMoneyInfo3];
    
}

- (IBAction)addMoney:(id)sender {
    
    if([self isMoneyZero] || !isFirstDeal) {
        return;
    }
    
    if(allMoney > stake) {
        stake += 100;
    }
    
    [self printMoneyInfo];
}

- (IBAction)removeMoney:(id)sender {
    
    if([self isMoneyZero] || !isFirstDeal) {
        return;
    }
    
    if(stake > 100) {
        stake -= 100;
        
    }
    
    [self printMoneyInfo];

}

- (NSMutableArray *) makeDeck {
    
    deck = [[NSMutableArray alloc] initWithCapacity:54];
    
    for (int k = 1; k < 3; k++) {
        
        Card *joker = [[Card alloc] init];
        
        joker.num = 0;
        joker.sign = @"joker";
        
        NSString *img = [NSString stringWithFormat:@"joker%i", k];
        joker.image = [UIImage imageNamed:img];
    
        [joker.image setAccessibilityIdentifier:img];
        
        [deck addObject:joker];
        
    }
    
    for (int j = 0; j < 4; j++) {
        
        NSString *sign;
        
        if(j==0) {
            sign = @"H";
        } else if (j==1) {
            sign = @"S";
        } else if (j==2) {
            sign = @"D";
        } else {
            sign = @"C";
        }
        
        for (int i = 2; i < 15; i++) {
            
            Card *myCard = [[Card alloc] init];
            
            myCard.num = i;
            myCard.sign = sign;
            
            NSString *img = [NSString stringWithFormat:@"%i%@.png", i, sign];
            myCard.image = [UIImage imageNamed:img];
 
            [myCard.image setAccessibilityIdentifier:img];
            
            [deck addObject:myCard];
            
        }
    }
    
    NSMutableArray *retVal = deck;
    
    return retVal;
}

- (void) printMoneyInfo {
    
    _moneyInfo.text = [NSString stringWithFormat:@"CREDIT $: %i", allMoney];
    _betMoney.text = [NSString stringWithFormat:@"BET $: %i", stake];
    
}

- (void) printMoneyInfo2 {
    
    _moneyInfo.text = [NSString stringWithFormat:@"CREDIT $: %i", allMoney];
    _betMoney.text = [NSString stringWithFormat:@"BET $: %i", stake];
    _win.text = [NSString stringWithFormat:@"WIN $: %i", bet];

    
}

- (void) printMoneyInfo3 {
    
    _moneyInfo.text = [NSString stringWithFormat:@"CREDIT $: %i", allMoney];
    _betMoney.text = [NSString stringWithFormat:@"BET $: %i", stake];
    _win.text = @"";
    _cardCombination.text = @"";
}

- (void) disableEnableMainBtns:(bool)param {
    
    _gambleGame.enabled = !param;
    _finishBtn.enabled = !param;
    _deal.enabled = param;
    
    _addMoney.enabled = param;
    _removeMoney.enabled = param;
    _betAllBtn.enabled = param;
    
}

- (void) disableEnableBtns:(bool)param {
    
    _card0.enabled = param;
    _card1.enabled = param;
    _card2.enabled = param;
    _card3.enabled = param;
    _card4.enabled = param;
    
    _addMoney.enabled = !param;
    _removeMoney.enabled = !param;
    _betAllBtn.enabled = !param;
    
}

- (bool) isMoneyZero {
    
    if(allMoney < 100) {
        _cardCombination.text = @"G A M E   O V E R";
    
        _deal.enabled = NO;
        _addMoney.enabled = NO;
        _removeMoney.enabled = NO;
        _betAllBtn.enabled = NO;
    
        return true;
    }
    
    return false;
}

// Display our 5 Cards; there can't be 2 or more same Cards on screen - for it functionality we use array
// indexes in which we put indexes of our five Cards from the main deck; array indexes can't hold 2 same
// numbers

- (void) showCards {
    
    ourFiveCards = [[NSMutableArray alloc] initWithCapacity:5];
    
    int a;
    
    NSMutableArray *indexes = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < 5; i++) {
        
        UIButton *btn;
        
        a = arc4random_uniform(54);
        
        while([indexes containsObject:@(a)]) {
            a = arc4random_uniform(54);
        }
        
        [indexes addObject:[NSNumber numberWithInteger:a]];
        
        Card *card1 = deck[a];
        
        switch(i){
            case 0:
                btn = self.card0;
                break;
            case 1:
                btn = self.card1;
                break;
            case 2:
                btn = self.card2;
                break;
            case 3:
                btn = self.card3;
                break;
            case 4:
                btn = self.card4;
        }
        
        NSString *img = [card1.image accessibilityIdentifier];
        
        [btn setBackgroundImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
        
        [ourFiveCards addObject:card1];
    }
    
}

// Sort Cards in ascending way

- (NSArray*) sortCards {
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"num"
                                                 ascending:YES];
    NSArray *sortedArray = [ourFiveCards sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    return sortedArray;
    
}

// Replace all Cards from array "cardsForReplace" (there can't be 2 same Cards)

- (void) replaceCards{
    
    for (int i = 0; i < cardsForReplace.count; i++) {
        
        UIButton *btn = cardsForReplace[i];
        
        Card *card1;
        
        if(btn  == _card0) {
            
            card1 = [self checkCard:0];
            [ourFiveCards replaceObjectAtIndex:0 withObject:card1];
            
        } else if (btn  == _card1) {
            
            card1 = [self checkCard:1];
            [ourFiveCards replaceObjectAtIndex:1 withObject:card1];
            
        } else if (btn  == _card2) {
            
            card1 = [self checkCard:2];
            [ourFiveCards replaceObjectAtIndex:2 withObject:card1];
            
        } else if (btn  == _card3) {
           
            card1 = [self checkCard:3];
            [ourFiveCards replaceObjectAtIndex:3 withObject:card1];
            
        } else if (btn  == _card4) {
           
            card1 = [self checkCard:4];
            [ourFiveCards replaceObjectAtIndex:4 withObject:card1];
            
        }
        
        NSString *img = [card1.image accessibilityIdentifier];
        
        [btn setBackgroundImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
        
    }
}

// Calculate score for different Card combinations

- (int) calculateWin {
    
    NSArray *sortedCards = [self sortCards];
    
    Card *first = sortedCards[0];
    Card *second = sortedCards[1];
    Card *third = sortedCards[2];
    Card *fourth = sortedCards[3];
    Card *fifth = sortedCards[4];
    
    int sign = 1;
    
    for (int i = 1; i < 5; i++) {
        
        Card *card = sortedCards[i];
        
        if([card.sign isEqualToString:first.sign]) {
            sign++;
        }
    }
    
    // Five of a kind
    
    if(first.num == 0 && second.num == third.num && third.num == fourth.num && fourth.num == fifth.num) {
        
        _cardCombination.text = @"FIVE OF A KIND";
        
        return 2000;
        
    }
    
    // Straight flush
    
    if(sign == 5 && first.num == second.num - 1 && second.num == third.num - 1 && third.num == fourth.num - 1 && fourth.num == fifth.num - 1) {
        
         // Royal straight flush
        
            if(first.num == 10) {
                
                _cardCombination.text = @"ROYAL STRAIGHT FLUSH";

                return 1200;
                
            }
        _cardCombination.text = @"STRAIGHT FLUSH";
        
        return 1000;
    }
    
    // Straight flush low
    
    if(sign == 5 && first.num == second.num - 1 && second.num == third.num - 1 && third.num == fourth.num - 1 && fifth.num == 11 && first.num == 2) {
        
        _cardCombination.text = @"STRAIGHT FLUSH LOW";
        
        return 900;
    }
    
    int num1 = 1;
    int num2 = 1;
    int num3 = 1;
    int num4 = 1;
    
    Card *cardNum;
    
    for (int i = 2; i < 5; i++) {
        
        cardNum = sortedCards[i];
        
        if(i == 2 && first.num == second.num && ![first.sign  isEqual: @"joker"]) {
            num1++;
        }
        
        if(![cardNum.sign isEqual: @"joker"]) {
            
            if(cardNum.num == first.num) {
                ++num1;
                continue;
            }
            
            if(cardNum.num == second.num) {
                ++num2;
                continue;
            }
            
            if(i > 2 && cardNum.num == third.num) {
                ++num3;
                continue;
            }
            
            if(i == 4 && cardNum.num == fourth.num) {
                ++num4;
            }
        }
    }
    
    // Four of a kind
    
    if(num1 == 4 || num2 == 4) {
        
        _cardCombination.text = @"FOUR OF A KIND";
        
        return 800;
    }
    
    // Full house
    
    if((num1 == 3 && num2 == 2) || (num1 == 2 && num2 == 3) || (num1 == 2 && num3 == 3)) {
        
        _cardCombination.text = @"FULL HOUSE";

        return 700;
    }
    
    // Flush
    
    if(sign == 5) {
        
        _cardCombination.text = @"FLUSH";
        
        return 600;
    }
    
    // Straight
    
    if(first.num == second.num - 1 && second.num == third.num - 1 && third.num == fourth.num - 1 && fourth.num == fifth.num - 1) {
        
        _cardCombination.text = @"STRAIGHT";

        return 500;
    }
    
    // Straight low
    
    if(first.num == second.num - 1 && second.num == third.num - 1 && third.num == fourth.num - 1 && fifth.num == 11 && first.num == 2) {
        
        _cardCombination.text = @"STRAIGHT LOW";
        
        return 400;
    }
    
    // Three of a kind
    
    if(num1 == 3 || num2 == 3 || num3 == 3) {
    
        _cardCombination.text = @"THREE OF A KIND";
        
        return 300;
    }
    
    // Two pair
    
    if((num1 == 2 && num2 == 2) || (num1 == 2 && num3 == 2) || (num1 == 2 && num4 == 2) || (num2 == 2 && num3 == 2) || (num2 == 2 && num4 == 2) || (num3 == 2 && num4 == 2)) {
        
        _cardCombination.text = @"TWO PAIR";
        
        return 200;
    }
    
    // One pair
    
    if(num1 == 2 || num2 == 2 || num3 == 2 || num4 == 2) {
        
        _cardCombination.text = @"ONE PAIR";
        
        return 100;
    }
    
    _cardCombination.text = @"";
    
    return 0;
}

// Show/hide all labels "HELD" at the same time
- (void) showHideHeldLbls:(BOOL)yesOrNo {
    
    [_held0 setHidden:yesOrNo];
    [_held1 setHidden:yesOrNo];
    [_held2 setHidden:yesOrNo];
    [_held3 setHidden:yesOrNo];
    [_held4 setHidden:yesOrNo];

}

// F-on that shows/hides (toggle) label "HELD" when we click on Card and also put it Card in array
// "cardsForReplace"

- (void) replaceOneCard:(UIButton*)btn andLbl:(UILabel*)lbl {
    
    if(![cardsForReplace containsObject:btn]) {
        
        [cardsForReplace addObject:btn];
        [lbl setHidden:YES];
        
    } else {
        
        [cardsForReplace removeObject:btn];
        [lbl setHidden:NO];
        
    }
    
}

// This f-on return Card for replace that is not already on display in our five Cards
// Card can be replaced with the same Card

- (Card*) checkCard: (int)escapeNum {
    
    int rd = arc4random_uniform(54);
    
    Card *newCard = deck[rd];
    
    BOOL isCardInDeck = true;
    
    while(isCardInDeck) {
        
        isCardInDeck = false;
    
        for (int i = 0; i < ourFiveCards.count; i++) {
            
            if(i == escapeNum) {
                continue;
            }
            
            Card *it = ourFiveCards[i];
            
            if (newCard.num == it.num && [newCard.sign isEqualToString:it.sign]) {
                isCardInDeck = true;
                
                rd = arc4random_uniform(54);
                
                newCard = deck[rd];
                
                break;
            }
        }
    }
    
    return newCard;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier]isEqualToString:@"i1"]) {
        SecondViewController *svc = [segue destinationViewController];
        
        svc.bet = bet;
        
        svc.allMoney = allMoney;        
    }
}


@end


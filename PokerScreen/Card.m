//
//  Card.m
//  Poker
//
//  Created by Sanja on 8/21/19.
//  Copyright © 2019 Sanja. All rights reserved.
//

#import "Card.h"
#import <UIKit/UIKit.h>

@implementation Card

- (id)initMethod:(int)num andSign:(NSString*)sign andImage:(UIImage*)image{
    
    self = [super init];
    
    if (self) {
        self.num = num;
        self.sign = sign;
        self.image = image;
    }
    
    return self;
}

@end

//
//  SecondViewController.h
//  PokerScreen
//
//  Created by Sanja Pantić on 24/08/19.
//  Copyright © 2019 Sanja Pantić. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstViewController.h"
#import "Card.h"

@interface SecondViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *money;

@property (weak, nonatomic) IBOutlet UIButton *credit;
@property (weak, nonatomic) IBOutlet UIButton *finish;

@property (weak, nonatomic) IBOutlet UIImageView *cardImage;

@property (nonatomic) int bet;
@property (nonatomic) int allMoney;

- (IBAction)low:(id)sender;
- (IBAction)hi:(id)sender;
- (IBAction)finish:(id)sender;

- (Card *) chooseRndCard;
- (void) printInfo;


@end


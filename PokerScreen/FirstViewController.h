//
//  FirstViewController.h
//  PokerScreen
//
//  Created by Sanja Pantić on 24/08/19.
//  Copyright © 2019 Sanja Pantić. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"

@interface FirstViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *moneyInfo;
@property (weak, nonatomic) IBOutlet UILabel *cardCombination;
@property (weak, nonatomic) IBOutlet UILabel *win;
@property (weak, nonatomic) IBOutlet UILabel *betMoney;

@property (weak, nonatomic) IBOutlet UIButton *card0;
@property (weak, nonatomic) IBOutlet UIButton *card1;
@property (weak, nonatomic) IBOutlet UIButton *card2;
@property (weak, nonatomic) IBOutlet UIButton *card3;
@property (weak, nonatomic) IBOutlet UIButton *card4;

@property (weak, nonatomic) IBOutlet UILabel *held0;
@property (weak, nonatomic) IBOutlet UILabel *held1;
@property (weak, nonatomic) IBOutlet UILabel *held2;
@property (weak, nonatomic) IBOutlet UILabel *held3;
@property (weak, nonatomic) IBOutlet UILabel *held4;

@property (weak, nonatomic) IBOutlet UIButton *deal;
@property (weak, nonatomic) IBOutlet UIButton *addMoney;
@property (weak, nonatomic) IBOutlet UIButton *removeMoney;
@property (weak, nonatomic) IBOutlet UIButton *gambleGame;
@property (weak, nonatomic) IBOutlet UIButton *finishBtn;
@property (weak, nonatomic) IBOutlet UIButton *betAllBtn;

@property (nonatomic) int bet;
@property (nonatomic) int allMoney;

- (IBAction)replace0:(UIButton *)sender;
- (IBAction)replace1:(UIButton *)sender;
- (IBAction)replace2:(UIButton *)sender;
- (IBAction)replace3:(UIButton *)sender;
- (IBAction)replace4:(UIButton *)sender;

- (IBAction)betAll:(id)sender;
- (IBAction)finishGame:(id)sender;

- (IBAction)dealCards:(id)sender;
- (IBAction)addMoney:(id)sender;
- (IBAction)removeMoney:(id)sender;
- (IBAction)goToGambleGame:(id)sender;

- (NSMutableArray *) makeDeck;
- (void) printMoneyInfo;
- (void) printMoneyInfo2;
- (void) printMoneyInfo3;
- (void) disableEnableMainBtns:(bool)param;
- (void) disableEnableBtns:(bool)param;
- (bool) isMoneyZero;
- (void) showCards;
- (int) calculateWin;
- (NSArray*) sortCards;
- (void) replaceCards;
- (void) showHideHeldLbls:(BOOL)yesOrNo;
- (void) replaceOneCard:(UIButton*)btn andLbl:(UILabel*)lbl;
- (Card*) checkCard: (int)escapeNum;



@end


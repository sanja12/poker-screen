//
//  AppDelegate.h
//  PokerScreen
//
//  Created by Sanja Pantić on 24/08/19.
//  Copyright © 2019 Sanja Pantić. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  Card.h
//  Poker
//
//  Created by Sanja on 8/21/19.
//  Copyright © 2019 Sanja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Card : NSObject

@property (assign) int num;
@property (assign) NSString* sign;
@property (nonatomic, strong) UIImage *image;

- (id)initMethod:(int)num andSign:(NSString*)sign andImage:(UIImage*)image;

@end

NS_ASSUME_NONNULL_END
